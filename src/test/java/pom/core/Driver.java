package pom.core;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Optional;
import pom.core.utilities.PropertyManager;

import java.net.MalformedURLException;
import java.net.URL;

public class Driver {

    public DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
    public AndroidDriver<MobileElement> androidDriver = null;
    public WebDriverWait wait;

    final String defaultDeviceName = "emulator";
    String appiumNodeServerUrl;

    // Below method accepts an @Optional argument i.e. deviceName,
    // if it is not passed by the calling method 'defaultDeviceName' will be used for "deviceName" capability.
    public AndroidDriver<MobileElement> AndroidCapabilities(@Optional(defaultDeviceName) String deviceName) throws MalformedURLException {

        if(deviceName.equals(PropertyManager.EMULATOR_NAME)) {
            appiumNodeServerUrl = PropertyManager.NODE_SERVER_URL_EMULATOR;
            desiredCapabilities.setCapability("deviceName", PropertyManager.EMULATOR_NAME);
            desiredCapabilities.setCapability("udid", PropertyManager.EMULATOR_UDID);
        }

        if(deviceName.equals(PropertyManager.DEVICE1_NAME)) {
            appiumNodeServerUrl = PropertyManager.NODE_SERVER_URL_DEVICE1;
            desiredCapabilities.setCapability("deviceName", PropertyManager.DEVICE1_NAME);
            desiredCapabilities.setCapability("udid", PropertyManager.DEVICE1_UDID);
        }

        if(deviceName.equals(PropertyManager.DEVICE2_NAME)) {
            appiumNodeServerUrl = PropertyManager.NODE_SERVER_URL_DEVICE2;
            desiredCapabilities.setCapability("deviceName", PropertyManager.DEVICE2_NAME);
            desiredCapabilities.setCapability("udid", PropertyManager.DEVICE2_UDID);
        }

        desiredCapabilities.setCapability("app", PropertyManager.APK_DOWNLOAD_URL);
        desiredCapabilities.setCapability("automationName", PropertyManager.AUTOMATION_NAME);
        desiredCapabilities.setCapability("platformName", PropertyManager.PLATFORM_NAME);
        desiredCapabilities.setCapability("autoGrantPermissions", true);
        desiredCapabilities.setCapability("noReset", true);
        desiredCapabilities.setCapability("unicodeKeyboard", true);
        desiredCapabilities.setCapability("resetKeyboard", true);
        desiredCapabilities.setCapability("appPackage", PropertyManager.APP_PACKAGE_NAME);
        desiredCapabilities.setCapability("appActivity", PropertyManager.APP_ACTIVITY_NAME);
        desiredCapabilities.setCapability("appWaitActivity", PropertyManager.APP_WAITACTIVITY_NAME);

        androidDriver = new AndroidDriver<>(new URL(appiumNodeServerUrl), desiredCapabilities);
        wait = new WebDriverWait(androidDriver, 15);
        return androidDriver;
    }
}