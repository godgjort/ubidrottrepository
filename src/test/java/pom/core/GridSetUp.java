package pom.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class GridSetUp{

    Runtime runtime = Runtime.getRuntime();

    File gridRunnerScriptsLocation = new File("src/test/resources/gridRunners");
    File gridStopperScriptsLocation = new File("src/test/resources/gridStoppers");

    File startHubScript = new File(gridRunnerScriptsLocation, "hubRunner");
    File stopHubScript = new File(gridStopperScriptsLocation, "hubStopper");

    File startNodeScriptAndDev1 = new File(gridRunnerScriptsLocation, "nodeRunnerAndroidDevice1");
    File stopNodeScriptAndDev1 = new File(gridStopperScriptsLocation, "nodeStopperAndroidDevice1");

    File startNodeScriptAndDev2 = new File(gridRunnerScriptsLocation, "nodeRunnerAndroidDevice2");
    File stopNodeScriptAndDev2 = new File(gridStopperScriptsLocation, "nodeStopperAndroidDevice2");

    File startNodeScriptEmulator = new File(gridRunnerScriptsLocation, "nodeRunnerEmulator");
    File stopNodeScriptEmulator = new File(gridStopperScriptsLocation, "nodeStopperEmulator");

    List<File> startGridScripts = Arrays.asList(startHubScript, startNodeScriptEmulator, startNodeScriptAndDev1);
    List<File> stopGridScripts = Arrays.asList(stopHubScript, stopNodeScriptEmulator, stopNodeScriptAndDev1);

    public void startGridHubAndNode(){
        int bound = startGridScripts.size();
        IntStream.rangeClosed(1, bound).forEachOrdered(i -> {
            try {
                ProcessBuilder pb = new ProcessBuilder("bash", startGridScripts.get(i).toString());
                pb.inheritIO();
                Process process = pb.start();
                process.waitFor(10, TimeUnit.SECONDS);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public void stopGridHubAndNode(){
        int bound = startGridScripts.size();
        IntStream.rangeClosed(0, bound).forEachOrdered(i -> {
            try {
                ProcessBuilder pb = new ProcessBuilder("bash", stopGridScripts.get(i).toString());
                pb.inheritIO();
                Process process = pb.start();
                process.waitFor(10, TimeUnit.SECONDS);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public void startHubServer() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", startHubScript.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stopHubServer() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", stopHubScript.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startNodeServerAndDev1() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", startNodeScriptAndDev1.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stopNodeServerAndDev1() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", stopNodeScriptAndDev1.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startNodeServerAndDev2() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", startNodeScriptAndDev2.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stopNodeServerAndDev2() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", stopNodeScriptAndDev2.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startNodeServerEmulator() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", startNodeScriptEmulator.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stopNodeServerEmulator() {
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", stopNodeScriptEmulator.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor(10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
