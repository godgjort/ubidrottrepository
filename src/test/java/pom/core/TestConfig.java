// @BeforeSuite >> will be executed before any tests declared inside a TestNG suite.
// @BeforeTest >> will be executed before each test section declared inside a TestNG suite.
// @BeforeClass >> will be executed before first @Test method execution. It runs only once per class.
// @BeforeMethod >> will be executed before every @Test annotated method.
// @BeforeGroups >> will run before any of the test method of the specified group is executed. groups attribute must contain the list of groups this method belongs to.
// @AfterSuite >> will be executed after any tests declared inside a TestNG suite.
// @AfterTest >> will be executed after each test section declared inside a TestNG suite.
// @AfterClass >> will be executed after the last test method of a test class.
// @AfterMethod >> will be executed after the execution of each test method.
// @AfterGroups >> will run after the last test method of the specified group is executed. groups attribute must contain the list of groups this method belongs to.

package pom.core;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.annotations.*;
import pom.core.utilities.PropertyManager;

import java.io.IOException;
import java.net.MalformedURLException;

public class TestConfig extends Driver {

    public AndroidDriver<MobileElement> androidDriver;
    GridSetUp gridSetUp = new GridSetUp();

    @BeforeSuite
    public void setUpBeforeSuite() {
        gridSetUp.startHubServer();
        gridSetUp.startNodeServerEmulator();
        gridSetUp.startNodeServerAndDev1();
    }

    @Parameters({"deviceName"})
    @BeforeTest
    public void setUpBeforeTest(String deviceName) throws IOException {
        // Check if hub and nodes are connected/responding
        UtilMethods utilMethods = new UtilMethods();
        boolean isGridHubWorking = utilMethods.getHttpStatusForHub();
        boolean isGridNodeWorking = utilMethods.getHttpStatusForNodeServer(deviceName);

        if (!isGridHubWorking || !isGridNodeWorking) {
            System.out.println("!!! Grid SetUp is not working !!!");
        } else {
            System.out.println("* * * Grid SetUp is working * * *");
        }
        PropertyManager.loadAutomationProperties();
    }

    @Parameters({"deviceName"})
    @BeforeClass
    public void setUpBeforeClass(String deviceName) throws MalformedURLException {
        System.out.println("! ! ! ! ! Instantiating androidDriver @BeforeClass ! ! ! ! !");

        // Instantiating an object of 'AndroidCapabilities()' class.
         androidDriver = AndroidCapabilities(deviceName);
    }

    @AfterClass
    public void tearDownAfterClass() {
        System.out.println("* * * * * Quitting androidDriver @AfterClass * * * * *");
        androidDriver.quit();
    }

    @AfterTest
    public void tearDownAfterTest() {
    }

    @AfterSuite
    public void tearDownAfterSuite() {
        gridSetUp.startHubServer();
        gridSetUp.startNodeServerEmulator();
        gridSetUp.startNodeServerAndDev1();
        System.out.println(">>>>>>>>>>>> Shut Down Hub and Node Servers @AfterSuite <<<<<<<<<<<<");
    }
}

