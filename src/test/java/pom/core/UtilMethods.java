package pom.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Optional;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class UtilMethods {

    private static Logger logger = LogManager.getLogger();

    boolean gridSetUpStatus;

    final String defaultDeviceName = "emulator";  // 'redminote5', 'motog4', 'emulator'

    String hubServerUrl = "http://127.0.0.1:4444/wd/hub/status";
    String nodeServerUrl;
    String nodeServerEmulator = "http://127.0.0.1:4723/wd/hub/status";
    String nodeServerAndDev1Url = "http://127.0.0.1:4733/wd/hub/status";
    String nodeServerAndDev2Url = "http://127.0.0.1:4743/wd/hub/status";

    int httpResponseCode;

    // Below method will return TRUE if GridHub Server is responding 200.
    public boolean getHttpStatusForHub() throws IOException {
        URL url = new URL(hubServerUrl);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        httpResponseCode = connection.getResponseCode();
        if (httpResponseCode != 200) {
            logger.debug("Could not get the '200 OK' response from the Grid Server Url i.e. " + hubServerUrl);
            System.out.println("Could not get the '200 OK' response from the Grid Server Url i.e. " + hubServerUrl);
        } else {
            logger.debug("httpResponseCode for requested Server Url i.e. " + hubServerUrl + " is: >> "+ httpResponseCode);
            System.out.println("httpResponseCode for requested Server Url i.e. " + hubServerUrl + " is: >> "+ httpResponseCode);
        }
        return (httpResponseCode == 200);
    }

    public boolean getHttpStatusForNodeServer(@Optional(defaultDeviceName) String deviceName) throws IOException {

        if(deviceName.equals("emulator")) {
            nodeServerUrl = nodeServerEmulator;
        }
        if(deviceName.equals("redminote5")) {
            nodeServerUrl = nodeServerAndDev1Url;
        }
        if(deviceName.equals("redmiy2")) {
            nodeServerUrl = nodeServerAndDev2Url;
        }

        URL url = new URL(nodeServerUrl);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        httpResponseCode = connection.getResponseCode();
        if (httpResponseCode != 200) {
            logger.debug("<" + deviceName + "> Could not get the '200 OK' response from the Node Server Url i.e. " + nodeServerUrl);
            System.out.println("<" + deviceName + "> Could not get the '200 OK' response from the Node Server Url i.e. " + nodeServerUrl);
        } else {
            logger.debug("<" + deviceName + "> httpResponseCode for requested Server Url i.e. " + nodeServerUrl + " is: >> "+ httpResponseCode);
            System.out.println("<" + deviceName + "> httpResponseCode for requested Server Url i.e. " + nodeServerUrl + " is: >> "+ httpResponseCode);
        }
        return (httpResponseCode == 200);
    }
}
