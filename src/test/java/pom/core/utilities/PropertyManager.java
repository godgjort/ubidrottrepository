package pom.core.utilities;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public  class PropertyManager {

    private static FileReader fileReader;

    public static String EMULATOR_NAME;
    public static String EMULATOR_UDID;
    public static String DEVICE1_NAME;
    public static String DEVICE1_UDID;
    public static String DEVICE2_NAME;
    public static String DEVICE2_UDID;
    public static String APK_DOWNLOAD_URL;
    public static String AUTOMATION_NAME;
    public static String PLATFORM_NAME;
    public static String APP_PACKAGE_NAME;
    public static String APP_ACTIVITY_NAME;
    public static String APP_WAITACTIVITY_NAME;
    public static String NODE_SERVER_URL_EMULATOR;
    public static String NODE_SERVER_URL_DEVICE1;
    public static String NODE_SERVER_URL_DEVICE2;

    private static void configFileReader() throws IOException {
        File propertiesFile = new File(System.getProperty("user.dir")+"/src/test/resources/properties/configuration.properties");
        fileReader = new FileReader(propertiesFile);
    }

    public static void loadAutomationProperties() {

        Properties configurationProperties = new Properties();

        try { configFileReader(); }
        catch (IOException e) { e.printStackTrace(); }

        try {
            configurationProperties.load(fileReader);
            fileReader.close();
        }
        catch (IOException e) { e.printStackTrace(); }

        try {
            EMULATOR_NAME =  configurationProperties.getProperty("emulatorName");
            EMULATOR_UDID = configurationProperties.getProperty("emulatorUdid");
            DEVICE1_NAME = configurationProperties.getProperty("device1Name");
            DEVICE1_UDID = configurationProperties.getProperty("device1Udid");
            DEVICE2_NAME = configurationProperties.getProperty("device2Name");
            DEVICE2_UDID = configurationProperties.getProperty("device2Udid");
            APK_DOWNLOAD_URL = configurationProperties.getProperty("apkDownloadUrl");
            AUTOMATION_NAME = configurationProperties.getProperty("automationName");
            PLATFORM_NAME = configurationProperties.getProperty("platformName");
            APP_PACKAGE_NAME = configurationProperties.getProperty("appPackageName");
            APP_ACTIVITY_NAME = configurationProperties.getProperty("appActivityName");
            APP_WAITACTIVITY_NAME = configurationProperties.getProperty("appWaitActivityName");
            NODE_SERVER_URL_EMULATOR = configurationProperties.getProperty("nodeServerUrlEmulator");
            NODE_SERVER_URL_DEVICE1 = configurationProperties.getProperty("nodeServerUrlDevice1");
            NODE_SERVER_URL_DEVICE2 = configurationProperties.getProperty("nodeServerUrlDevice2");
        } catch (Exception e) {
            System.out.println("Error in Assigning/Fetching the values from 'configurationProperties'");
            e.printStackTrace();
        }
    }
}