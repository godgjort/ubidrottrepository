package pom.pomActivities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.TimeUnit;

public class OmrMainActivity {

    public AndroidDriver<MobileElement> driver;

    public OmrMainActivity(AndroidDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver, 30, TimeUnit.SECONDS), this);
    }

    // Identifying and Storing Elements of MainActivity
    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_logo")
    public MobileElement loginLogoElement;

    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_title")
    public MobileElement loginTitleElement;

    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_username")
    public MobileElement loginUsernameElement;

    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_password")
    public MobileElement loginPasswordElement;

    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_forgot_password")
    public MobileElement loginForgotPasswordElement;

    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_login_group")
    public MobileElement loginLoginGroupElement;

    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_register")
    public MobileElement loginRegisterElement;

    @WithTimeout(time = 20, unit = TimeUnit.SECONDS)
    @AndroidFindBy(id="com.unibet.unibetpro:id/view_login_warning")
    public MobileElement loginWarningElement;

    public boolean validateMainActivityElements() {
        AtomicBoolean defaultElementsPresent = new AtomicBoolean(false);

        if(loginLogoElement.isDisplayed()) {
            if(loginTitleElement.isDisplayed()) {
                if(loginUsernameElement.isDisplayed()) {
                    if (loginPasswordElement.isDisplayed()) {
                        if (loginForgotPasswordElement.isDisplayed()) {
                            if (loginLoginGroupElement.isDisplayed()) {
                                if (loginRegisterElement.isDisplayed()) {
                                    defaultElementsPresent.set(true);
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            defaultElementsPresent.set(false);
        }
        return defaultElementsPresent.get();
    }

    public boolean validateLoginOnlyElements() {
        AtomicBoolean loginElementsPresent = new AtomicBoolean(false);

        if(loginUsernameElement.isDisplayed()) {
            System.out.println("\n###### loginUsernameElement.isDisplayed() ######\n");
            if (loginForgotPasswordElement.isDisplayed()) {
                System.out.println("\n###### loginForgotPasswordElement.isDisplayed() ######\n");
                if (loginLoginGroupElement.isDisplayed()) {
                    System.out.println("\n###### loginLoginGroupElement.isDisplayed() ######\n");
                    loginElementsPresent.set(true);
                }
            }
        } else {
            loginElementsPresent.set(false);
        }
        return loginElementsPresent.get();
    }
}