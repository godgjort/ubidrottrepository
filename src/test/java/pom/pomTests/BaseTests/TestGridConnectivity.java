package pom.pomTests.BaseTests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import pom.core.TestConfig;

import java.io.IOException;

public class TestGridConnectivity extends TestConfig {

    @Parameters({"deviceName"})
    @BeforeTest
    @Override
    public void setUpBeforeTest(String deviceName) throws IOException {
        super.setUpBeforeTest(deviceName);
    }
}