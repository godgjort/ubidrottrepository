package pom.pomTests.activities.mainActivityNgtvTests;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pom.core.TestConfig;
import pom.pomActivities.OmrMainActivity;

public class MainActivityEmptyCredentials extends TestConfig {

    // Test Empty Login Credentials
    @Test
    public void VerifyEmptyLoginCredentials() {

        String expectedErrMsgEmptyLogin = "Username or Password can't be empty!";
        String actualErrMsgEmptyLogin;

        if(androidDriver.currentActivity().endsWith(".activity.MainActivity")) {

            boolean loginElementsDisplayed;

            // Creating an Instance of 'OmrMainActivity' Class, passing 'androidDriver' as an argument
            OmrMainActivity omrMainActivity = new OmrMainActivity(androidDriver);
            loginElementsDisplayed = omrMainActivity.validateLoginOnlyElements();

            if(loginElementsDisplayed) {
                omrMainActivity.loginUsernameElement.clear();
                omrMainActivity.loginPasswordElement.clear();
                omrMainActivity.loginLoginGroupElement.click();
            }

            wait.until(ExpectedConditions.visibilityOf(omrMainActivity.loginWarningElement));
            actualErrMsgEmptyLogin = omrMainActivity.loginWarningElement.getText();

            //Verify correct message is displayed in case of 'Empty Login Credentials'
            Assert.assertEquals(actualErrMsgEmptyLogin, expectedErrMsgEmptyLogin);
        }
        else {
            System.out.println("hmmm: you know, you are running tests in a wrong Activity");
        }
    }
}