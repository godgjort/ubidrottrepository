package pom.pomTests.activities.mainActivityNgtvTests;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.*;
import pom.core.TestConfig;
import pom.pomActivities.OmrMainActivity;

import java.util.concurrent.TimeUnit;

public class MainActivityInvalidCredentials extends TestConfig {

    // Test Incorrect Login Credentials
    @Test
    public void VerifyInvalidLoginCredentials() {

        String expectedErrMsgInvalidLogin = "Invalid username or password";
        String actualErrMsgInvalidLogin;

        if(androidDriver.currentActivity().endsWith(".activity.MainActivity")) {

            boolean loginElementsDisplayed;

            // Creating an Instance of 'OmrMainActivity' Class, passing 'androidDriver' as an argument
            OmrMainActivity omrMainActivity = new OmrMainActivity(androidDriver);
            loginElementsDisplayed = omrMainActivity.validateLoginOnlyElements();

            if(loginElementsDisplayed) {
                omrMainActivity.loginUsernameElement.clear();
                omrMainActivity.loginUsernameElement.sendKeys("invalidUserId@gmail.com");
                omrMainActivity.loginPasswordElement.clear();
                omrMainActivity.loginPasswordElement.sendKeys("InvalidP@ssword1234");
                omrMainActivity.loginLoginGroupElement.click();
            }

            androidDriver.manage().timeouts().implicitlyWait( 15, TimeUnit.SECONDS);    // Wait for CAPTCHA to disappear
            wait.until(ExpectedConditions.visibilityOf(omrMainActivity.loginWarningElement)).isDisplayed();
            actualErrMsgInvalidLogin = omrMainActivity.loginWarningElement.getText();

            //Verify correct message is displayed in case of 'Invalid Login Credentials'
            Assert.assertEquals(actualErrMsgInvalidLogin, expectedErrMsgInvalidLogin);
        }
        else {
            System.out.println("hmmm: you know, you are running tests in a wrong Activity");
        }
    }
}