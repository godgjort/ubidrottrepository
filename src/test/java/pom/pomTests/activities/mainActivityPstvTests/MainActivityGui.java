package pom.pomTests.activities.mainActivityPstvTests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;
import pom.core.TestConfig;
import pom.pomActivities.OmrMainActivity;

public class MainActivityGui extends TestConfig {
    private static Logger logger = LogManager.getLogger();

    @Test
    public void VerifyMainActivityGui() {
        // COMPLETED: Implement log4j instead of sout
        logger.debug("Current Activity is: " + androidDriver.currentActivity());

        boolean defaultElementsDisplayed;

        if(androidDriver.currentActivity().endsWith(".activity.MainActivity")) {
            // Creating an Instance of 'OmrMainActivity' Class, passing 'androidDriver' as an argument
            OmrMainActivity omrMainActivity = new OmrMainActivity(androidDriver);
            defaultElementsDisplayed = omrMainActivity.validateMainActivityElements();
        }
        else {
            defaultElementsDisplayed = false;
            logger.error("hmmm: you know, you are running tests in a wrong Activity");
        }
        Assert.assertTrue(defaultElementsDisplayed);
    }
}