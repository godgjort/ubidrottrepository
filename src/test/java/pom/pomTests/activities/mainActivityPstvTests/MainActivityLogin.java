package pom.pomTests.activities.mainActivityPstvTests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pom.core.TestConfig;
import pom.pomActivities.OmrMainActivity;

public class MainActivityLogin extends TestConfig {

    // Test Correct Login Credentials
    @Test
    public void VerifyValidLoginCredentials() {

        if(androidDriver.currentActivity().endsWith(".activity.MainActivity")) {

            boolean loginElementsDisplayed;

            // Creating an Instance of 'OmrMainActivity' Class, passing 'androidDriver' as an argument
            OmrMainActivity omrMainActivity = new OmrMainActivity(androidDriver);
            loginElementsDisplayed = omrMainActivity.validateLoginOnlyElements();

            if(loginElementsDisplayed) {
                omrMainActivity.loginUsernameElement.clear();
                omrMainActivity.loginUsernameElement.sendKeys("unibet_115615434");
                omrMainActivity.loginPasswordElement.clear();
                omrMainActivity.loginPasswordElement.sendKeys("Yadmin369369");
                omrMainActivity.loginLoginGroupElement.click();
            }
            Assert.assertTrue(loginElementsDisplayed);
        }
        else {
            System.out.println("hmmm: you know, you are running tests in a wrong Activity");
        }
    }
}
